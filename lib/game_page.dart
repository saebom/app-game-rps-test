import 'package:flutter/material.dart';
import 'dart:math';  //랜덤진행할때 진행.

class GamePage extends StatefulWidget {
  const GamePage({super.key, required this.title});

  final String title; //파이널은 필수므로 위에 const에 대한 코드를 바꿔줘야함.

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {

  int _gamePoint = 10000;   //10000에 대한 기능추가
  int _countWin = 0;
  int _countDraw = 0;
  int _countLose = 0;
  String _gameMessage = ""; //기본값은 없음으로 추가

  int? _myChoice = null;
  int? _comChoice = null;


  //이미지를 바꾸는 코드
  final List<Map> _gameItem = [
    {'name' : '가위', 'imgSrc' : 'assets/a.png'},
    {'name' : '바위', 'imgSrc' : 'assets/b.png'},
    {'name' : '보', 'imgSrc' :  'assets/c.png'},
  ];

  void _gameStart(int itemIndex) {
    setState(() {
      if (_gamePoint < 1000) {
        _gameMessage = "포인트가 부족해요! Reset 해요!";
      } else {
        _myChoice = itemIndex;
      _comChoice = Random().nextInt(_gameItem.length);

      if ((_myChoice == 0 && _comChoice == 1) || (_myChoice == 1 && _comChoice == 2)
          || (_myChoice == 2 && _comChoice == 0)) {
      _countLose += 1;
      _gamePoint -= 1000;
      _gameMessage = "졌어요! 1000점을 잃었어요!";
      } else if ((_myChoice == 0 && _comChoice == 2) || (_myChoice == 1 && _comChoice == 0)) {
      _countWin += 1;
      _gamePoint += 1000;
      _gameMessage = "이겼어요! 1000점을 얻었어요!";
      } else {
      _countDraw += 1;
      _gameMessage = "비겼어요! 아무것도 없어요!";
      }
    }
    });
  }
  void _gameReset() {
    setState(() {
      _gamePoint = 10000;
      _countWin = 0;
      _countDraw = 0;
      _countLose = 0;
      _gameMessage = "";

      _myChoice = null;
      _comChoice = null;
    });
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 30,
            color: Colors.black54,
        ),
        ),
      ),
      body: Column( //맨위 컬럼 추가
        mainAxisAlignment: MainAxisAlignment.spaceAround, //줄맞춤
        children: [
          Text("Point : $_gamePoint", //다른거 추가할땐 무조건 ,찍고 진행.
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 30,
              color: Colors.deepOrange
            ),
          ), //위에 int _gamePont를 만들고 텍스트에서 수정!

           //줄추가
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text("win : $_countWin",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.amber,
                    height: 1.5
                ),
              ),
              Text("draw : $_countDraw",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.amber,
                    height: 1.5
                ),
              ),
              Text("lose : $_countLose",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.amber,
                    height: 1.5
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround, //줄맞춤
            children: [
              OutlinedButton(
                  onPressed: () {
                _gameStart(0);
              },
                  child: Text("가위",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.deepOrange,
                  ),
                  )
              ),//3번째줄 버튼으로 진행.
              OutlinedButton(
                  onPressed: () {
                _gameStart(1);
              },
                  child:
                  Text("바위",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.deepOrange,
                    ),
                  )
              ),
              OutlinedButton(
                  onPressed: () {
                _gameStart(2);
              },
                  child: Text("보",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.deepOrange,
                    ),
                  )
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column( //row안에 column을 또 넣음.
                children: [
                  Text("나"),  //컬럼이 2개 들어감 : Text,Image
                  Image.asset( //이미지 사진을 넣음
                    "${_myChoice == null ? 'assets/d.png' : _gameItem[_myChoice!]['imgSrc']}",
                    width: 100, //폭 사이즈
                    height: 80, //높이 사이즈
                  )
                ],
              ),
              Text("VS"),
              Column(
                children: [
                  Text("COM"),
                  Image.asset(
                    "${_comChoice == null ? 'assets/d.png' : _gameItem[_comChoice!]['imgSrc']}",
                    width: 100, //폭 사이즈
                    height: 80, //높이 사이즈
                  )
                ],
              )
            ],
          ),
          Text(_gameMessage),
          OutlinedButton(
              onPressed: () {
            _gameReset();},
              child: Text("Reset",
              style: TextStyle(
                fontSize: 20,
                color: Colors.deepOrange,
              )
              )
          ),
          Container(
            child: Row(
              children: [
                SizedBox(
                  width: 500,
                  height: 26,
                  child: ElevatedButton.icon(
                    onPressed: null,
                    label: Text('kimsaebom.com'),
                    icon: Icon(Icons.face),
                  ),
                )
              ],
            ),
          ),
        ]
      ), //맨처음 컬럼진행.
    );
  }
}
